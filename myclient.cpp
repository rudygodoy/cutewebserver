#include "myclient.h"
#include <QHostAddress>
#include <QString>
#include <QStringList>
#include <QCryptographicHash>
#include "httprequestheader.h"

MyClient::MyClient(QObject *parent) :
    QObject(parent)
{
    QThreadPool::globalInstance()->setMaxThreadCount(65000);
}

void MyClient::SetSocket(int Descriptor)
{
    socket = new QTcpSocket(this);
    connect(socket,SIGNAL(connected()),this,SLOT(connected()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(disconnected()));
    connect(socket,SIGNAL(readyRead()),this,SLOT(readyRead()));

    socket->setSocketDescriptor(Descriptor);
    qDebug() << "Client connected.";
}

void MyClient::connected()
{
    qDebug() << "Client connected event.";
    // record cookie timestamp
}
void MyClient::disconnected()
{
    qDebug() << "Client disconnected.";
    // record cookie timestamp
}
void MyClient::readyRead()
{
    qDebug() << "Client readyRead event.";
    QString clientIpAddress = socket->peerAddress().toString();
    QString clientHTTPRequest = socket->readAll();
    HttpRequestHeader * request = new HttpRequestHeader(clientHTTPRequest);
    QString clientUserAgent = request->getUserAgent();
    QString clientUri = request->getUri();
    QString clientUniqueIdString = clientIpAddress.append(clientUserAgent);
    QByteArray clientUniqueId = QCryptographicHash::hash(clientUniqueIdString.toAscii(),QCryptographicHash::Md5).toHex();
    qDebug() << clientUniqueId;
    qDebug() << clientUri;
    qDebug() << request->getFullUri();

    MyTask * mytask = new MyTask();
    //
    mytask->setAutoDelete(true);
    connect(mytask,SIGNAL(Result(int)),this,SLOT(TaskResult(int)),Qt::QueuedConnection);
    QThreadPool::globalInstance()->start(mytask);

    // time comsumer  - deadlock
}
void MyClient::TaskResult(int Number)
{
   // qDebug() << "Client TaskResult.";
    QByteArray Buffer;


    Buffer.append("HTTP/1.1 200 OK\r\n\
Server: Apache/2.0.52 (CentOS)\r\n\
Accept-Ranges: bytes\r\n\
Content-Length: 1000\r\n\
Content-Type: text/html; charset=ISO-8859-1\r\n\
\r\n\
data data data data data \
<html><body><b>Test data</b></body></html> \
");

    Buffer.append("Task Result  = ");
    Buffer.append(QString::number(Number));

    socket->write(Buffer);
    socket->flush();
    socket->waitForBytesWritten();
    socket->disconnectFromHost();
    socket->close();

}










