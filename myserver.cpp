#include "myserver.h"

Myserver::Myserver(QObject *parent) :
    QTcpServer(parent)
{
}


void Myserver::StartServer(int port=8080)
{
    if ( this->listen(QHostAddress::Any,port))
    {
        qDebug() << "Server started";
    }
    else
    {
        qDebug() << "Server No started";
    }
}

void Myserver::incomingConnection(int handle)
{
    MyClient * client = new MyClient(this);
    client->SetSocket(handle);
    qDebug() << "Connection closed";
}
