#include <QCoreApplication>
#include "myserver.h"
#include <QStringList>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QStringList args;
    args = a.arguments();
    int port = 8080;
    if (args.count() > 1)
        port = args.at(1).toInt(0);

    Myserver server;
    server.StartServer(port);

    return a.exec();
}
