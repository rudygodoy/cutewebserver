#include "httprequestheader.h"

HttpRequestHeader::HttpRequestHeader()
{

}

HttpRequestHeader::HttpRequestHeader(QString buffer)
{
   QStringList clientRawReq = buffer.split("\r");
   for (int i = 0; i < clientRawReq.size(); ++i){
       if (clientRawReq.at(i).startsWith("GET")){
           setFullUri(clientRawReq.at(i));
           setUri(getFullUri());
       }
       if (clientRawReq.at(i).startsWith("POST")){
           setFullUri(clientRawReq.at(i));
           setUri(getFullUri());
       }
       if (clientRawReq.at(i).startsWith("Host:")){
           setHost(clientRawReq.at(i));
       }
       if (clientRawReq.at(i).startsWith("Accept:")){
           setAccept(clientRawReq.at(i));
       }
       if (clientRawReq.at(i).startsWith("Accept-Language:")){
           setAcceptLanguage(clientRawReq.at(i));
       }
       if (clientRawReq.at(i).startsWith("Accept-Encoding:")){
           setAcceptEncoding(clientRawReq.at(i));
       }
       if (clientRawReq.at(i).startsWith("Connection:")){
           setConnection(clientRawReq.at(i));
       }

//       qDebug() << clientRawReq.at(i).toLocal8Bit().constData();
   }
}


QString HttpRequestHeader::getFullUri(){
    return fullUri;
}

QString HttpRequestHeader::getUri(){
    return uri;
}

QString HttpRequestHeader::getHost(){
    return host;
}

QString HttpRequestHeader::getUserAgent(){
    return userAgent;
}

QString HttpRequestHeader::getAccept(){
    return accept;
}

QString HttpRequestHeader::getAcceptLanguage(){
    return acceptLanguage;
}

QString HttpRequestHeader::getAcceptEncoding(){
    return acceptEncoding;
}

QString HttpRequestHeader::getConnection(){
    return connection;
}

void HttpRequestHeader::setFullUri(QString fullUri){
    this->fullUri = fullUri;
}

void HttpRequestHeader::setUri(QString furi){
    QStringList uri = furi.split(QRegExp("\\s"));
    this->uri = uri.at(1);
}

void HttpRequestHeader::setHost(QString host){
    this->host = host;
}

void HttpRequestHeader::setUserAgent(QString userAgent){
    this->userAgent = userAgent;
}

void HttpRequestHeader::setAccept(QString accept){
    this->accept = accept;
}

void HttpRequestHeader::setAcceptLanguage(QString acceptLanguage){
    this->acceptLanguage = acceptLanguage;
}

void HttpRequestHeader::setAcceptEncoding(QString acceptEncoding){
    this->acceptEncoding = acceptEncoding;
}

void HttpRequestHeader::setConnection(QString connection){
     this->connection = connection;
}
