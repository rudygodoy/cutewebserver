#ifndef HTTPREQUESTHEADER_H
#define HTTPREQUESTHEADER_H
#include <QString>
#include <QDebug>
#include <QStringList>


class HttpRequestHeader
{

private:
    QString fullUri;
    QString uri;
    QString host;
    QString userAgent;
    QString accept;
    QString acceptLanguage;
    QString acceptEncoding;
    QString connection;

public:
    HttpRequestHeader();
    HttpRequestHeader(QString buffer);

    QString getFullUri();
    QString getUri();
    QString getHost();
    QString getUserAgent();
    QString getAccept();
    QString getAcceptLanguage();
    QString getAcceptEncoding();
    QString getConnection();

    void setFullUri(QString);
    void setUri(QString);
    void setHost(QString);
    void setUserAgent(QString);
    void setAccept(QString);
    void setAcceptLanguage(QString);
    void setAcceptEncoding(QString);
    void setConnection(QString);

};

#endif // HTTPREQUESTHEADER_H
