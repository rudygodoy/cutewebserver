#-------------------------------------------------
#
# Project created by QtCreator 2013-09-10T09:32:42
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = cuteWebserver
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    myserver.cpp \
    myclient.cpp \
    mytask.cpp \
    httprequestheader.cpp

HEADERS += \
    myserver.h \
    myclient.h \
    mytask.h \
    httprequestheader.h
