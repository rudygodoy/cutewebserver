#ifndef MYSERVER_H
#define MYSERVER_H

#include <QTcpServer>
#include <QTcpSocket>
#include <QAbstractSocket>
#include "myclient.h"

class Myserver : public QTcpServer
{
    Q_OBJECT
public:
    explicit Myserver(QObject *parent = 0);
    void StartServer(int port);
protected:
    void incomingConnection(int handle);
signals:

public slots:

};

#endif // MYSERVER_H
